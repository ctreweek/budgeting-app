import { Component, OnInit } from '@angular/core';
import { Expense } from '../expense';
import { ExpenseItemsService } from '../expense-items.service';
import { FormControl, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-expense-list',
  templateUrl: './expense-list.component.html',
  styleUrls: ['./expense-list.component.scss']
})
export class ExpenseListComponent implements OnInit {
  expenses: Expense[] = [];
  categories = [];
  expenseTableRows = this.expenses;

  selectCategory = new FormGroup({
    category: new FormControl()
  });

  onSelect(event) {
    let selectedCategory:string = this.selectCategory.value.category;
    this.expenseTableRows = [];
    for (let i = 0; i < this.expenses.length; i++) {
      if (this.expenses[i].category === selectedCategory) {
        this.expenseTableRows.push(this.expenses[i]);
      }
    }
  }

  constructor(public expenseItemsService: ExpenseItemsService) { }

  ngOnInit(): void {
    this.expenseItemsService.expenseSubject.subscribe(expense => {
      this.expenses.push(expense);
    })

    this.expenseItemsService.categoriesSubject.subscribe(category => {
      this.categories.push(category);
    })
  }

}

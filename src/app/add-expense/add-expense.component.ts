import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Expense } from '../expense';
import { CalculateService } from '../calculate.service';
import { ExpenseItemsService } from '../expense-items.service';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.scss']
})
export class AddExpenseComponent implements OnInit {
  public totalExpenses:number = 0;
  public categories;

  expenseForm = new FormGroup({
    expense: new FormControl(),
    amount: new FormControl(),
    category: new FormControl()
  });

  onSubmit(expenseName:string, expenseAmount:number, expenseCategory:string) {
    let newExpense: Expense = {name: expenseName, amount: expenseAmount, category: expenseCategory};
    this.totalExpenses += +expenseAmount;
    this.calculateService.totalExpenses = this.totalExpenses;
    this.calculateService.calculateBalance(this.totalExpenses, this.calculateService.budget);
    this.expenseItemsService.expenseSubject.next(newExpense);
  }

  categoryListHandler(event) {
    this.categories = event;
  }

  constructor(private calculateService: CalculateService,
              private expenseItemsService: ExpenseItemsService) { }

  ngOnInit(): void {
  }

}

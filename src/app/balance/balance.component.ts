import { Component, OnInit } from '@angular/core';
import { CalculateService } from '../calculate.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {
  remainingBalance:number;

  constructor(private calculateService: CalculateService) {}

  ngOnInit(): void {
    this.calculateService.remainingBalanceSubject.subscribe(balance => {
      this.remainingBalance = balance;
    })
  }

}

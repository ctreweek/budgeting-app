import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryDialogComponent } from '../category-dialog/category-dialog.component';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ExpenseItemsService } from '../expense-items.service';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  categories: string[] = ["Housing/Utilities", "Transportation", "Food", "Fun"];
  category: string;

  //allow categoryList to flow to parent component (add-expense)
  @Output() categoryList = new EventEmitter();

  constructor(private dialog: MatDialog, public expenseItemsService: ExpenseItemsService) {

  }

  openDialog() {
        const dialogConfig = new MatDialogConfig();

        const dialogRef = this.dialog.open(CategoryDialogComponent, {
          data: {category: this.category, categories: this.categories}
        });

        dialogRef.afterClosed().subscribe(result => {
          this.category = result;
          if (result !== undefined) {
            this.categories.push(result);

            //send category list to parent component (add-expense)
            this.categoryList.emit(this.categories);

            this.expenseItemsService.categoriesSubject.next(result);
            this.category = "";
          }
        });
      }

  ngOnInit(): void {
    this.categoryList.emit(this.categories);
    for ( let i = 0; i < this.categories.length; i++) {
        this.expenseItemsService.categoriesSubject.next(this.categories[i]);
    }

  }

}

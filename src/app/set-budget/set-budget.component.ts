import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CalculateService } from '../calculate.service';


@Component({
  selector: 'app-set-budget',
  templateUrl: './set-budget.component.html',
  styleUrls: ['./set-budget.component.scss']
})
export class SetBudgetComponent implements OnInit {
  budget: number;
  remainingBalance: number;
  budgetControl = new FormControl('');
  @Input() public totalExpenses:number;

  setBudget(budgetTotal:number) {
    this.budget = budgetTotal;
    this.calculateService.budget = this.budget;
    this.calculateService.calculateBalance(this.calculateService.totalExpenses, this.budget);
  }

  constructor(private calculateService: CalculateService) { }

  ngOnInit(): void {
  }

}

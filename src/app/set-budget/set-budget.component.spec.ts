import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetBudgetComponent } from './set-budget.component';

describe('BudgetComponent', () => {
  let component: SetBudgetComponent;
  let fixture: ComponentFixture<SetBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetBudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

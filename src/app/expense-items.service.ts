import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Expense } from './expense';


@Injectable({
  providedIn: 'root'
})
export class ExpenseItemsService {
  public expenseCategories = [];
  public expenseItems = [];
  expenseSubject = new ReplaySubject<Expense>();
  categoriesSubject = new ReplaySubject();


  constructor() { }
}

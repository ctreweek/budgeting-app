import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class CalculateService {
  public totalExpenses:number = 0;
  public budget: number;

  public remainingBalance:number;
  remainingBalanceSubject = new BehaviorSubject(this.budget);

  calculateBalance(totalExpenses:number, budget:number) {
    this.remainingBalance = budget - totalExpenses;
    this.remainingBalanceSubject.next(this.remainingBalance);
  }


  constructor() { }
}

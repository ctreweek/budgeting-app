import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { SetBudgetComponent } from './set-budget/set-budget.component';
import { AddExpenseComponent } from './add-expense/add-expense.component';
import { ExpenseListComponent } from './expense-list/expense-list.component';
import { BalanceComponent } from './balance/balance.component';
import { CategoryDialogComponent } from './category-dialog/category-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { CategoryComponent } from './category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    SetBudgetComponent,
    AddExpenseComponent,
    ExpenseListComponent,
    BalanceComponent,
    CategoryDialogComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatInputModule,
  ],

  exports: [ MatFormFieldModule, MatInputModule ],

  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CategoryDialogComponent]
})
export class AppModule { }
